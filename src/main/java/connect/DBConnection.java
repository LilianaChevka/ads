package connect;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;


public class DBConnection {

    private static final Logger logger = LogManager.getLogger(DBConnection.class);
    private static Properties properties = new Properties();
    private static Connection connection = null;

    public DBConnection() {
        loadProperties();
    }

    public static Connection getConnection() {
        String url = "jdbc:postgresql://localhost:5432/AdvertDB"; // properties.getProperty("url");
        String username = "postgres"; //properties.getProperty("username");
        String password = "root";  //properties.getProperty("password");
        try {
            connection = DriverManager.getConnection(url, username, password);
        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
        return connection;
    }

    private boolean loadProperties() {
        ClassLoader classLoader = getClass().getClassLoader();
        try (InputStream input = classLoader.getResourceAsStream("db.properties");) {
            if (input != null) {
                properties.load(input);
            }
            return true;
        } catch (IOException e) {
            logger.error(e.getMessage());
            return false;
        }
    }

    public boolean executeUpdate(String query) {
        try (Statement statement = getConnection().createStatement()) {
            statement.executeUpdate(query);
            return true;
        } catch (Exception e) {
            logger.error(e.getMessage());
            return false;
        }
    }
}
