package connect;

public class TableUtils {

    private final DBConnection dbConnection;

    public TableUtils(DBConnection dbConnection) {
        this.dbConnection = dbConnection;
    }

    public void createTables() {

        String queryCreateTableUsers = "create table if not exists users " +
                "( id serial primary key, first_name varchar ," +
                " last_name varchar, email varchar unique, " +
                "date_of_birth date, role varchar(10), is_active_account boolean, password varchar); ";

        String queryCreateTableAdverts = "create table if not exists adverts " +
                "( id serial primary key, title varchar(20), description varchar(200)," +
                " author int references users (id) on delete cascade on update cascade,date_of_publication date, duration_of_publication date," +
                " is_active boolean, heading varchar(20));";

        dbConnection.executeUpdate(queryCreateTableUsers);
        dbConnection.executeUpdate(queryCreateTableAdverts);
    }

    public void dropTables() {
        String queryUsers = "drop table if exists users cascade";
        String queryAdverts = "drop table if exists adverts";
        dbConnection.executeUpdate(queryUsers);
        dbConnection.executeUpdate(queryAdverts);
    }
}
