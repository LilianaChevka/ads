package main;

import connect.DBConnection;
import connect.TableUtils;
import model.Advert;
import model.Heading;
import model.Role;
import model.User;
import repository.AdvertRepository;
import repository.UserRepository;

import java.time.LocalDate;

public class Main {

    public static void main(String[] args) {
        DBConnection dbConnection = new DBConnection();
        TableUtils tableUtils = new TableUtils(dbConnection);
        tableUtils.createTables();

        User user1 = new User("John", "Doe", "johndoe@gmail.com", LocalDate.now(), true, Role.ADMIN,"1223");
        User user2 = new User("Tom", "Clark", "tomclark@gmail.com", LocalDate.now(), true, Role.USER,"12235");
        User user3 = new User("Kate", "Adams", "kateadams@gmail.com", LocalDate.now(), true, Role.USER,"22344");
        User user4 = new User("Sharon", "Stone", "sharonstone@gmail.com", LocalDate.now(), false, Role.ANONYMOUS,"234266");

        UserRepository userRepository = new UserRepository(dbConnection);

        userRepository.save(user1);
        userRepository.save(user2);
        userRepository.save(user3);
        userRepository.save(user4);
        System.out.println(userRepository.findAll());
        System.out.println(user1);
        System.out.println(user2);

        Advert advert1 = new Advert("Title 1", "Description1", user1, LocalDate.now(), Heading.WELLNESS, true);
        Advert advert2 = new Advert("Title 2", "Description2", user2, LocalDate.now(), Heading.SPORT, true);
        Advert advert3 = new Advert("Title 3", "Description3", user3, LocalDate.now(), Heading.CARS, true);
        Advert advert4 = new Advert("Title 4", "Description4", user4, LocalDate.now(), Heading.IMMOBILITY, true);

        AdvertRepository advertRepository = new AdvertRepository(dbConnection);

        advertRepository.save(advert1);
        advertRepository.save(advert2);
        advertRepository.save(advert3);
        advertRepository.save(advert4);

        System.out.println(advertRepository.findAll());
        System.out.println(advert1);
    }

}