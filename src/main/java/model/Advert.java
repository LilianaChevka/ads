package model;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;


public class Advert implements Serializable {
    private static final long serialVersionUID = 1L;


    @NotNull
    @Min(1)
    private Long id;

    @Min(6)
    @Max(20)
    private String title;

    @Min(20)
    @Max(200)
    private String description;

    @NotNull
    @Min(1)
    private User author;

    @NotNull
    private LocalDate dateOfPublication;

//    @NotNull
//    private LocalDate durationOfPublication;

    @NotNull
    private Heading heading;

    @NotNull
    private Boolean isActive;

    public Advert() {

    }

    public Advert(String title, String description, User author, LocalDate dateOfPublication, Heading heading, boolean isActive) {
        super();
        this.title = title;
        this.description = description;
        this.author = author;
        this.dateOfPublication = dateOfPublication;
//        this.durationOfPublication = durationOfPublication;
        this.heading = heading;
        this.isActive = isActive;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public LocalDate getDateOfPublication() {
        return dateOfPublication;
    }

    public void setDateOfPublication(LocalDate dateOfPublication) {
        this.dateOfPublication = dateOfPublication;
    }
//    public LocalDate getDurationOfPublication() {
//        return durationOfPublication;
//    }
//
//    public void setDurationOfPublication(@NotNull LocalDate durationOfPublication) {
//        this.durationOfPublication = durationOfPublication;
//    }

    public Heading getHeading() {
        return heading;
    }

    public void setHeading(Heading heading) {
        this.heading = heading;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    @Override
    public String toString() {
        return "Advert{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", author=" + author +
                ", dateOfPublication=" + dateOfPublication +
//                ", durationOfPublication=" + durationOfPublication +
//                ", heading=" + heading +
                ", isActiveAccount=" + isActive +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Advert)) return false;
        Advert advert = (Advert) o;
        return id.equals(advert.id) && title.equals(advert.title)
                && description.equals(advert.description) && author.equals(advert.author)
                && dateOfPublication.equals(advert.dateOfPublication)
//                && durationOfPublication.equals(advert.durationOfPublication)
//                && heading == advert.heading
                && isActive.equals(advert.isActive);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, description,
                author, dateOfPublication, isActive);
    }
}