package model;

import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;


public class User implements Serializable {
    private static final long serialVersionUID = 1L;

    @NotNull
    private Long id;

    @Min(1)
    @Max(20)
    @NotNull
    private String firstName;

    @Min(1)
    @Max(20)
    @NotNull
    private String lastName;

    @Min(1)
    @Max(20)
    @NotNull
    @Email
    private String email;

    @NotNull
    private LocalDate dateOfBirth;

    @NotNull
    private boolean isActiveAccount;

    @NotNull
    private Role role;

    @NotNull
    private List<Advert> adverts;

    @NotNull
    private String password;

    public User() {
    }

    public User(String firstName, String lastName,
                String email, LocalDate dateOfBirth,
                boolean isActiveAccount, Role role, String password) {
        super();
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.dateOfBirth = dateOfBirth;
        this.isActiveAccount = isActiveAccount;
        this.role = role;
        this.password = password;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public boolean isActiveAccount() {
        return isActiveAccount;
    }

    public void setActiveAccount(boolean activeAccount) {
        isActiveAccount = activeAccount;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public List<Advert> getAdverts() {
        return adverts;
    }

    public void setAdverts(List<Advert> adverts) {
        this.adverts = adverts;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", dateOfBirth=" + dateOfBirth +
                ", isActiveAccount=" + isActiveAccount +
                ", role=" + role +
                ", adverts=" + adverts +
                '}';
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof model.User)) return false;
        model.User user = (model.User) o;
        return isActiveAccount == user.isActiveAccount && id.equals(user.id) && firstName.equals(user.firstName)
                && lastName.equals(user.lastName) && email.equals(user.email) && dateOfBirth.equals(user.dateOfBirth)
                && role == user.role && adverts.equals(user.adverts) && Objects.equals(password, user.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, firstName, lastName, email, dateOfBirth, isActiveAccount, role, adverts, password);
    }
}
