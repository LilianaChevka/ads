package repository;

import connect.DBConnection;
import model.Advert;
import model.Heading;
import model.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class AdvertRepository extends CrudOperationsRepo<Advert> {
    private final Logger logger = LogManager.getLogger(AdvertRepository.class);

    public AdvertRepository(DBConnection dbConnection) {
        super(dbConnection);
    }

    @Override
    public Advert save(Advert advert) {
        String queryInsert = "insert into adverts(title, description, heading, date_of_publication, is_active, author) " +
                "values(?, ?, ?, ?, ?, ?)";

        try (Connection connection = dbConnection.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(queryInsert, Statement.RETURN_GENERATED_KEYS)) {

            preparedStatement.setString(1, advert.getTitle());
            preparedStatement.setString(2, advert.getDescription());
            preparedStatement.setString(3, advert.getHeading().toString());
//            preparedStatement.setDate(4, Date.valueOf(advert.getDateOfPublication()));
            preparedStatement.setDate(4, null);
//            preparedStatement.setBoolean(5, advert.getActive());
            preparedStatement.setBoolean(5, false);
//            preparedStatement.setLong(6, advert.getAuthor().getId());
            preparedStatement.setLong(6, 1);
            preparedStatement.executeUpdate();
            ResultSet result = preparedStatement.getGeneratedKeys();
            while (result.next()) {
                advert.setId((long) result.getInt(1));
            }
            result.close();
        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
        return advert;
    }

    @Override
    public Advert update(Advert advert) {
        String queryUpdate = "update adverts set title=?, description=?, heading=?, is_active=? where id=?";
        try (PreparedStatement preparedStatement = dbConnection.getConnection().prepareStatement(queryUpdate)) {
            preparedStatement.setString(1, advert.getTitle());
            preparedStatement.setString(2, advert.getDescription());
            preparedStatement.setString(3, advert.getHeading().toString());
            preparedStatement.setBoolean(4, advert.getActive());
            preparedStatement.setLong(5, advert.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            logger.error(e.getLocalizedMessage());
        }
        return advert;
    }

    @Override
    public boolean delete(Long id) {
        String query = "delete from adverts where id=?";
        try (PreparedStatement preparedStatement = dbConnection.getConnection().prepareStatement(query)) {
            preparedStatement.setLong(1, id);
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            logger.error(e.getLocalizedMessage());
            return false;
        }
    }

    @Override
    public Advert findById(Long id) {
        String query = "select * from adverts where id=?";
        Advert advert = null;
        try (PreparedStatement preparedStatement = dbConnection.getConnection().prepareStatement(query)) {
            preparedStatement.setLong(1, id);
            ResultSet result = preparedStatement.executeQuery();
            while (result.next()) {
                advert = getObject(result);
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return advert;
    }

    @Override
    public List<Advert> findAll() {
        List<Advert> adverts = new ArrayList<>();
        String query = "select * from adverts";
        try (ResultSet result = dbConnection.getConnection().createStatement().executeQuery(query)) {
            while (result.next()) {
                adverts.add(getObject(result));
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return adverts;
    }

    public Advert getObject(ResultSet result) {
        Advert advert = null;
        try {
            advert = new Advert();
            advert.setId((long) result.getInt("id"));
            advert.setTitle(result.getString("title"));
            advert.setDescription(result.getString("description"));
            advert.setHeading(Heading.valueOf(result.getString("heading")));
            advert.setActive(result.getBoolean("is_active"));
            advert.setDateOfPublication(new Date(result.getDate("date_of_publication").getTime()).toLocalDate());
            User user = new User();
            user.setId((long) result.getInt("author"));
            UserRepository userRepository = new UserRepository(dbConnection);
            user = userRepository.findById(user.getId());
            advert.setAuthor(user);
        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
        return advert;
    }

    @Override
    public Advert findByEmail(String email) {
        return null;
    }

}

