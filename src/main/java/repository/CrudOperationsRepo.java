package repository;

import connect.DBConnection;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public abstract class CrudOperationsRepo<T extends Serializable> {

    private final Logger logger = LogManager.getLogger(CrudOperationsRepo.class);
    protected DBConnection dbConnection;

    public CrudOperationsRepo(DBConnection dbConnection) {
        this.dbConnection = dbConnection;
    }

    public abstract T save(T t);

    public abstract T update(T t);

    public abstract boolean delete(Long id);

    public abstract T findById(Long id);

    public abstract List<T> findAll();

    public abstract T getObject(ResultSet result);

    public abstract T findByEmail(String email);



    public List<T> findByParamAndValueLike(String param, String value) {
        String query = "select * from adverts where " + param + " like '" + value + "'";
        return new ArrayList<>(execute(query));
    }

    public List<T> findByParamAndValueEquals(String param, String value) {
        String query = "select * from adverts where " + param + " = '" + value + "'";
        return new ArrayList<>(execute(query));
    }

    public List<T> findAllByDateByAsc() {
        String query = "select * from adverts order by date_of_publication asc";
        return new ArrayList<>(execute(query));
    }

    public List<T> findAllByDateByDesc() {
        String query = "select * from adverts order by date_of_publication desc";
        return new ArrayList<>(execute(query));
    }

    public List<T> findAllByParams(Map<String, String> fieldValueMap) {
        StringBuilder builder = new StringBuilder();
        if (!fieldValueMap.isEmpty()) {
            fieldValueMap.forEach((k, v) -> {
                if (builder.length() == 0) {
                    builder.append("where ");
                } else {
                    builder.append(" and ");
                }
                builder.append(" ").append(k).append(" like '").append(v).append("'");
            });
        }
        String query = "select * from users " + builder.toString();
        return new ArrayList<>(execute(query));
    }

    private List<T> execute(String query) {
        List<T> entities = new ArrayList<>();
        try (ResultSet result = dbConnection.getConnection().createStatement().executeQuery(query)) {
            while (result.next()) {
                entities.add(getObject(result));
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
        return entities;
    }
}
