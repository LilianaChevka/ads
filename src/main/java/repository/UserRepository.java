package repository;

import connect.DBConnection;
import model.Role;
import model.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class UserRepository extends CrudOperationsRepo<User> {
    private final Logger logger = LogManager.getLogger(UserRepository.class);

   /* public UserRepository() {
    }*/

    public UserRepository(DBConnection dbConnection) {
        super(dbConnection);
    }

    @Override
    public User save(User user) {
        String query = "insert into users( first_name, last_name, email, date_of_birth, role, is_active_account, password) " +
                "values( ?, ?, ?, ?, ?, ?, ?)";

        try (Connection connection = DBConnection.getConnection();
             PreparedStatement ps = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
            ps.setString(1, user.getFirstName());
            ps.setString(2, user.getLastName());
            ps.setString(3, user.getEmail());
            ps.setDate(4, Date.valueOf(user.getDateOfBirth()));
            ps.setString(5, user.getRole().toString());
            ps.setBoolean(6, user.isActiveAccount());
            ps.setString(7,user.getPassword());
            ps.executeUpdate();
            ResultSet result = ps.getGeneratedKeys();
            while (result.next()) {
                user.setId((long) result.getInt(1));
            }
            result.close();
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return user;
    }

    @Override
    public User update(User user) {
        String queryUpdate = "update users set first_name=?, last_name=?, email=?, role=?,is_active_account=?, password=? where id=?";
        try (PreparedStatement preparedStatement = DBConnection.getConnection().prepareStatement(queryUpdate)) {
            preparedStatement.setString(1, user.getFirstName());
            preparedStatement.setString(2, user.getLastName());
            preparedStatement.setString(3, user.getEmail());
            preparedStatement.setString(4, user.getRole().toString());
            preparedStatement.setBoolean(5, user.isActiveAccount());
            preparedStatement.setString(6, user.getPassword());
            preparedStatement.setLong(7, user.getId());

            preparedStatement.executeUpdate();
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return user;
    }

    @Override
    public boolean delete(Long id) {
        String query = "delete from users where id=?";
        try (PreparedStatement preparedStatement = dbConnection.getConnection().prepareStatement(query)) {
            preparedStatement.setLong(1, id);
            preparedStatement.executeUpdate();
            return true;
        } catch (Exception e) {
            logger.error(e.getLocalizedMessage());
            return false;
        }
    }

    @Override
    public User findById(Long id) {
        User user = new User();
        String query = "select * from users where id=?";
        try (PreparedStatement ps = DBConnection.getConnection().prepareStatement(query)) {
            ps.setLong(1, id);
            ResultSet result = ps.executeQuery();
            while (result.next()) {
                user = getObject(result);
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return user;
    }

    @Override
    public List<User> findAll() {
        List<User> users = new ArrayList<>();
        String query = "select * from users";
        try (ResultSet result = dbConnection.getConnection().createStatement().executeQuery(query)) {
            while (result.next()) {
                users.add(getObject(result));
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return users;
    }

    @Override
    public User getObject(ResultSet result) {
        User user = new User();
        try {
            user.setId((long) result.getInt("id"));
            user.setFirstName(result.getString("first_name"));
            user.setLastName(result.getString("last_name"));
            user.setEmail(result.getString("email"));
            user.setDateOfBirth(new Date(result.getDate("date_of_birth").getTime()).toLocalDate());
            user.setRole(Role.valueOf(result.getString("role")));
            user.setActiveAccount(result.getBoolean("is_active_account"));
            user.setPassword(result.getString("password"));
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return user;
    }

    @Override
    public User findByEmail(String email) {
        return new User();
    }
}
