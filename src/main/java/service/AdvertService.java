package service;

import model.Advert;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import repository.CrudOperationsRepo;

import java.util.List;


public class AdvertService implements CrudOperationsService<Advert> {
    private final Logger logger = LogManager.getLogger(AdvertService.class);

    private CrudOperationsRepo<Advert> repository;

    public AdvertService(CrudOperationsRepo<Advert> repository) {
        this.repository = repository;
    }

    @Override
    public Advert save(Advert advert) {
        return repository.save(advert);
    }

    @Override
    public Advert update(Advert advert) {
        return repository.update(advert);
    }

    @Override
    public boolean delete(Long id) {
        return repository.delete(id);
    }

    @Override
    public Advert findById(Long id) {
        return repository.findById(id);
    }

    @Override
    public List<Advert> findAll() {
        return repository.findAll();
    }

}
