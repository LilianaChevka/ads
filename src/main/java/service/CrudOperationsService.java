package service;

import java.io.Serializable;
import java.util.List;

public interface CrudOperationsService<T extends Serializable> {
    T save(T t);

    T update(T t);

    boolean delete(Long id);

    T findById(Long id);

    List<T> findAll();
}
