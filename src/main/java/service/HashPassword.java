package service;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.crypto.MacProvider;
import org.mindrot.jbcrypt.BCrypt;

import javax.crypto.SecretKey;

public class HashPassword {

    private final static int workload = 12;
    private final static SignatureAlgorithm SIGNATURE_ALGORITHM = SignatureAlgorithm.HS256;
    private final static SecretKey SECRET_KEY = MacProvider.generateKey(SIGNATURE_ALGORITHM);


    public static String hashPassword(String password) {
        String salt = BCrypt.gensalt(workload);
        return BCrypt.hashpw(password, salt);
    }

    public static boolean checkPassword(String password, String storedHash) {
        return BCrypt.checkpw(password, storedHash);
    }

    public static String createToken(final String password) {
        return Jwts.builder()
                .setSubject(password)
                .signWith(SIGNATURE_ALGORITHM, SECRET_KEY)
                .compact();
    }
}
