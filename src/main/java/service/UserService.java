package service;

import model.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import repository.CrudOperationsRepo;

import java.util.List;

public class UserService implements CrudOperationsService<User> {
    private final Logger logger = LogManager.getLogger(UserService.class);

    private CrudOperationsRepo<User> repository;

    public UserService(CrudOperationsRepo<User> repository) {
        this.repository = repository;
    }

    @Override
    public User save(User user) {
        return repository.save(user);
    }

    @Override
    public User update(User user) {
        return repository.update(user);
    }

    @Override
    public boolean delete(Long id) {
        return repository.delete(id);
    }

    @Override
    public User findById(Long id) {
        return repository.findById(id);
    }

    @Override
    public List<User> findAll() {
        return repository.findAll();
    }
}
