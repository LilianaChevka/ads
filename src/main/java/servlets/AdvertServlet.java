package servlets;

import connect.DBConnection;
import model.Advert;
import model.Heading;
import model.User;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import repository.AdvertRepository;
import repository.UserRepository;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;


@WebServlet(urlPatterns = {"/adverts", "/my-adverts", "/adverts/delete", "/adverts/edit", "/adverts/edited", "/adverts/add"})

public class AdvertServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private static final Logger logger = LogManager.getLogger(AdvertServlet.class);
    private final AdvertRepository advertRepository = new AdvertRepository(new DBConnection());
    private final UserRepository userRepository = new UserRepository(new DBConnection());
    ServletContext servletContext;
    private User user = new User();
    private Advert advert = new Advert();

    @Override
    public void init() {
        servletContext = getServletContext();
    }


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
        showAdverts(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
        Map<String, String[]> params = req.getParameterMap();
        if (params.containsKey("advertId")) {
            advert.setId(Long.parseLong(Arrays.toString(params.get("advertId"))));
        }
        if (params.containsKey("userId")) {
            user.setId(Long.parseLong(Arrays.toString(params.get("userId"))));
        }

        String action = req.getServletPath();

        try {
            switch (action) {
                case "/adverts/delete":
                    deleteAdvert(req, resp);
                    break;
                case "/adverts/edit":
                    updateAdvert(req, resp);
                    break;
                case "/adverts/add":
                    addAdvert(req, resp);
                    break;
                default:
                    showAdverts(req, resp);
                    break;
            }
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
        showAdverts(req, resp);
    }

    private void showAdverts(HttpServletRequest req, HttpServletResponse resp) {
        loadUser(req);
        String action = req.getServletPath();
        switch (action) {
            case "/adverts":
                showAdverts(req, resp, advertRepository.findAll());
                break;

        }
    }

    private void showAdverts(HttpServletRequest req, HttpServletResponse resp, List<Advert> adverts) {
        req.setAttribute("adverts", adverts);
        RequestDispatcher dispatcher = req.getRequestDispatcher("adverts.jsp");
        try {
            dispatcher.forward(req, resp);
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void loadUser(HttpServletRequest req) {
        Map<String, String[]> params = req.getParameterMap();

        String username = "";
        if (params.containsKey("username")) {
            username = Arrays.toString(params.get("username"));
        }
        if (user.getId() == null) {
            user = userRepository.findById(user.getId());
        }
        req.setAttribute("user", user);
    }

    private void loadAdvert(HttpServletRequest req) {
        Map<String, String[]> params = req.getParameterMap();
        if (params.containsKey("id")) {
            advert.setId(Long.parseLong(Arrays.toString(params.get("id"))));
        }
        advert = advertRepository.findById(advert.getId());
        req.setAttribute("advert", advert);
    }

    private void updateAdvert(HttpServletRequest req, HttpServletResponse resp) {
        loadAdvert(req);
        RequestDispatcher dispatcher = req.getRequestDispatcher("/editAdvert.jsp");
        try {
            dispatcher.forward(req, resp);
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void deleteAdvert(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        advertRepository.delete(advert.getId());
        redirectToAdverts(req, resp);
    }

    private void redirectToAdverts(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        req.setAttribute("adverts", advertRepository.findAll());
        resp.sendRedirect("/adverts");
    }

    //    private void addAdvert(HttpServletRequest req, HttpServletResponse resp) throws IOException {
//        advertRepository.save(req,userRepository.findById(user.getId()));
//        List<Advert> adverts = advertRepository.findAll();
//        req.setAttribute("adverts", adverts);
//        resp.sendRedirect( "/adverts");
//    }
    private void addAdvert(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        Advert advert = new Advert();
        advert.setTitle(req.getParameter("title"));
        advert.setDescription(req.getParameter("description"));
        advert.setHeading(Heading.valueOf(req.getParameter("heading")));

        advertRepository.save(advert);
//        List<Advert> adverts = advertRepository.findAll();
//        req.setAttribute("adverts", adverts);
        resp.sendRedirect("/adverts");
    }

    @Override
    public void destroy() {
        servletContext = null;
    }
}
