package servlets;

import connect.DBConnection;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import repository.UserRepository;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static service.HashPassword.checkPassword;
import static service.HashPassword.createToken;
import static service.HashPassword.hashPassword;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    public static ServletContext servletContext;
    private final Logger logger = LogManager.getLogger(LoginServlet.class);
    private UserRepository userRepository = new UserRepository(new DBConnection());
    private final String subject = "token";

    @Override
    public void init() throws ServletException {
        servletContext = getServletContext();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        RequestDispatcher rd = getServletContext().getRequestDispatcher("/login.jsp");
        rd.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username = req.getAttribute("username").toString();
        String password = req.getAttribute("password").toString();
        String hashedPassword = hashPassword(password);

        if (username.contains("@") || password.length() > 15 || password.length() < 5|| !checkPassword(password, hashedPassword)) {

            String token = createToken(subject);
            req.setAttribute("token", token);
        }
    }

    @Override
    public void destroy() {
        servletContext = null;
    }
}
