package servlets;

import connect.DBConnection;
import model.Role;
import model.User;
import repository.UserRepository;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;

import static service.HashPassword.hashPassword;

@WebServlet("/registration")
public class RegistrationServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
   private static final UserRepository userRepository = new UserRepository(new DBConnection());

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String password = request.getParameter("password");
        String hashedPassword = hashPassword(password);
            User user = new User(request.getParameter("firstname"), request.getParameter("lastname"), request.getParameter("email"), LocalDate.now(), true, Role.USER, hashedPassword);
            userRepository.save(user);
            RequestDispatcher view = request.getRequestDispatcher("/users.jsp");
            request.setAttribute("users", userRepository.findAll());
            try {
                view.forward(request, response);
            } catch (ServletException e) {
                e.printStackTrace();
            }

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("registration.jsp");
        dispatcher.forward(request, response);
    }
}


