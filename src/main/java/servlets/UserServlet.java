package servlets;

import connect.DBConnection;
import model.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import repository.UserRepository;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;


@WebServlet(urlPatterns = {"/users", "/users/delete", "/users/*", "/users/edit", "/users/edited" })
public class UserServlet extends HttpServlet {

    private final static Logger logger = LogManager.getLogger(UserServlet.class);
    private static final long serialVersionUID = 1L;

    UserRepository userRepository = new UserRepository(new DBConnection());

    public UserServlet() {
        super();
    }

    public static ServletContext servletContext;

    User user;

    @Override
    public void init() {
        servletContext = getServletContext();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String path;
        String pathInfo = request.getPathInfo();
        if (pathInfo == null) {
            request.setAttribute("users", userRepository.findAll());
            path = "/users.jsp";
        } else {
            long userId = Long.parseLong(pathInfo.substring(1));
            request.setAttribute("user", userRepository.findById(userId));
            path = "/editUser.jsp";
        }
        RequestDispatcher view = request.getRequestDispatcher(path);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) {

        String action = req.getServletPath();
        try {
            switch (action) {
                case "/users":
                    String pathInfo = req.getPathInfo();
                    long userId = Long.parseLong(pathInfo.substring(1));
                    updateUser(userId, req);
                    resp.sendRedirect("/users");
                    break;
                case "/users/delete":
                    Map<String, String[]> params = req.getParameterMap();
                    long id = -1L;
                    if (params.containsKey("id")) {
                        id = Long.parseLong(req.getParameter("id"));
                    }
                    userRepository.delete(id);
                    resp.sendRedirect("/users");
                    break;
                case "/registration":
                    saveUser(req, resp);
                    break;
                default:
                    showUsers(req, resp);
                    break;
            }
        } catch (IOException e) {
            logger.error(e.getMessage());
        } catch (ServletException e) {
            e.printStackTrace();
        }
    }

    private void showUsers(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher v = req.getRequestDispatcher("/users.jsp");
        List<User> users = userRepository.findAll();
        req.setAttribute("users", users);
        v.forward(req, resp);
    }

    private void updateUser(long id, HttpServletRequest req) {
        User user = userRepository.findById(id);
        user.setFirstName(req.getParameter("firstname"));
        user.setLastName(req.getParameter("lastname"));
        user.setEmail(req.getParameter("email"));
//        user.setPassword(req.getParameter("password")); //encription...
        userRepository.update(user);
    }

    private void saveUser(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        userRepository.save(user);
        resp.sendRedirect("/users");
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        User user = new User();
        userRepository.update(user);
        showUsers(req, resp);
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, NumberFormatException {
        Map<String, String[]> params = req.getParameterMap();

        long id = -1L;
        if (params.containsKey("id")) {
            id = Long.parseLong(Arrays.toString(params.get("id")));
        }
        userRepository.delete(id);
        showUsers(req, resp);
    }
}