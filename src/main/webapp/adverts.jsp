
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="uk">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0"
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.6.0/dist/umd/popper.min.js"
            integrity="sha384-KsvD1yqQ1/1+IA7gi3P0tyJcT3vR+NdBTt13hSJ2lnve8agRGXTTyNaBYmCR/Nwi"
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.min.js"
            integrity="sha384-nsg8ua9HAw1y0W1btsyWgBklPnCUAFLuTMS2G72MMONqmOymq585AcH49TLBQObG"
            crossorigin="anonymous"></script>
    <link rel="stylesheet" href="css/styles.css" type="text/css">
    <title> List of adverts </title>

    <div >
        <div >
            <a href="users" >Users</a>
            <a href="adverts" >Adverts</a>
        </div>
    </div>
</head>
<body>

<div >
    <form method="get" action="createAdvert.jsp">
        <button type="submit" name="button-add-advert">Add</button>
    </form>
    <div >
        <c:forEach items="${requestScope.adverts}" var="advert">
            <div >
                <h5 >${advert.title}</h5>
                <div >
                    <h5  align="right">${advert.heading}</h5>
                    <p  align="justify">${advert.description}</p>
                </div>
                <div >
                    <button type="button" >active</button>
                    <form action="adverts/edit" method="post">
                        <input type="hidden" name="advertId" value="${advert.id}">
                        <button type="submit" >Edit</button>
                    </form>
                    <form action="adverts/delete" method="post">
                        <input type="hidden" name="advertId" value="${advert.id}">
                        <button type="submit" >Delete</button>
                    </form>
                </div>
            </div>
        </c:forEach>
    </div>
</div>
</body>
</html>