<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="th" uri="http://jakarta.apache.org/taglibs/standard/permittedTaglibs" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<!DOCTYPE html>
<html lang="uk">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0"
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.6.0/dist/umd/popper.min.js"
            integrity="sha384-KsvD1yqQ1/1+IA7gi3P0tyJcT3vR+NdBTt13hSJ2lnve8agRGXTTyNaBYmCR/Nwi"
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.min.js"
            integrity="sha384-nsg8ua9HAw1y0W1btsyWgBklPnCUAFLuTMS2G72MMONqmOymq585AcH49TLBQObG"
            crossorigin="anonymous"></script>
    <link rel="stylesheet" href="css/styles.css" type="text/css">
    <title>Sign in</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
</head>
<body>
<%--<div class="login-container">--%>
<%--    <form class="form-login" method="post" action="adverts">--%>
<%--        <h1 class="label-sign-in">Log in</h1>--%>
<%--        <div class="container">--%>
<%--            <div class="sign-up-content">--%>
<%--                <div class="mb-4">--%>
<%--                    <label for="inputEmail" class="form-label">Please, enter your email address</label>--%>
<%--                    <input type="email" id="inputEmail" name="username">--%>
<%--                </div>--%>
<%--                <div class="mb-4">--%>
<%--                    <label for="inputPassword" class="form-label">Enter password</label>--%>
<%--                    <input type="password" id="inputPassword" name="password">--%>
<%--                </div>--%>
<%--                <button type="submit" class="btn btn-primary">Log in</button>--%>
<a>Log in</a>
<form class="form-signin" action="adverts" method="post">
    <a> Don`t have an account?</a> <a href="/registration"><b>Sign up</b></a>
    <br>
    <br>
    <div class="container text-left">
        <label for="username">Email address</label>:
        <input type="text" id="username" name="username" class="form-control" autofocus="autofocus"
               placeholder="Enter the email" required/> <br/>

        <label for="password">Password</label>:
        <input type="password" name="password" id="password" class="form-control"
               placeholder="Enter the password">
        <i class="far fa-eye" id="togglePassword"></i>
        <a  href="/changePassword" > Forgot my password </a>
    </div>
    <br/>
    <br>
    <button class="btn btn-lg btn-primary btn-block" type="submit" id="">Log in</button>
</form>
    <span c:if="${param.q != null}"></span>
                <span c:if="${param.size() == 1 && param.q == null}">
    <script>
        document.addEventListener('DOMContentLoaded', function () {
            errorMessageWithWrongEmailOrPassword();
        });
    </script>
</span>
<%--                <script type="text/javascript" c:src="@{/js/error_message_for_google_authentication.js}"></script>--%>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
                <script>
                    function errorMessageWithWrongEmailOrPassword() {
                        return swal({
                            title: "Error",
                            text: "The email address or password you've entered doesn't appear to exist. " +
                                "Please check your entry and try again.",
                            type: "error",
                            icon: "error",
                        });
                    }

                    const togglePassword = document.querySelector('#togglePassword');
                    const password = document.querySelector('#password');
                    togglePassword.addEventListener('click', function () {
                        const type = password.getAttribute('type') === 'password' ? 'text' : 'password';
                        password.setAttribute('type', type);
                        this.classList.toggle('fa-eye-slash');
                    });

                </script>

            </div>
        </div>


</div>
</body>
</html>
